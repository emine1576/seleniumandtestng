package com.codistan.Emine.lesson13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

public class IteratorExamples {

	public static void main(String[] args) {
		ArrayList<Integer>myAL = new ArrayList<>();
		myAL.add(2);
		myAL.add(13);
		myAL.add(1);
		myAL.add(57);
		myAL.add(6);
		System.out.println(myAL);
		Iterator myIterator = myAL.iterator();
		while(myIterator.hasNext()) {
			System.out.println(myIterator.next());
		
			String exStr = "Hello world, I would like to say hello to everyone because the world needs greetings and one of the good greeting way is saying hello."; 
			
			/*
			 * Find all the unique words and sort them and print each word line by line. 
			 */
			
			String[] words = exStr.split(" ");
			System.out.println(Arrays.toString(words) );
			
			TreeSet<String> myTS = new TreeSet<>(); 
			
			for (int i = 0; i < words.length; i++) {
				myTS.add(words[i]);
			}
			
			System.out.println(myTS);
			
			Iterator mySI = myTS.iterator();
			
			while(mySI.hasNext()) {
				System.out.println(mySI.next());
			}

		}

	}
}
			
			
